/*
 * drinks
 * architecture clients/serveur guinness : breuvages
 * Thomas Nemeth -- le 15 juin 2001
 *
 */


#ifndef GUINNNESS_BEVRG
#define GUINNNESS_BEVRG

char *drinks_get (const char *nom);
void drinks_list_files (const char *path);
void drinks_display_list ();

#endif

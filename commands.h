/*
 * commands
 * architecture clients/serveur guinness
 * Thomas Nemeth -- le 15 juin 2001
 *
 */


#ifndef SERVER_COMMANDS
#define SERVER_COMMANDS

/*#define MAXCMDS 14*/


typedef struct cmdslst {
    char *nom;
    char *aide;
    int   args;
    int   adm;
    int   interv;
    int  (*fct)();
} cmdslst;

int reply (int socket_service, char *commande, userinfos *infos);
int send_servercmds (int socket_service);

/* Fonctions associees aux commandes */
int f_help (int socket_service, const char *commande, userinfos *infos);
int f_quit (int socket_service, const char *commande, userinfos *infos);
int f_list (int socket_service, const char *commande, userinfos *infos);
int f_bevr (int socket_service, const char *commande, userinfos *infos);
int f_cold (int socket_service, const char *commande, userinfos *infos);
int f_turn (int socket_service, const char *commande, userinfos *infos);
int f_mesg (int socket_service, const char *commande, userinfos *infos);
int f_info (int socket_service, const char *commande, userinfos *infos);
int f_nick (int socket_service, const char *commande, userinfos *infos);
int f_glas (int socket_service, const char *commande, userinfos *infos);
int f_sadm (int socket_service, const char *commande, userinfos *infos);
int f_shut (int socket_service, const char *commande, userinfos *infos);
int f_rldb (int socket_service, const char *commande, userinfos *infos);
int f_addu (int socket_service, const char *commande, userinfos *infos);
int f_delu (int socket_service, const char *commande, userinfos *infos);
int f_save (int socket_service, const char *commande, userinfos *infos);
int f_load (int socket_service, const char *commande, userinfos *infos);


#endif

#
#		Guinness Server from tnemeth
#
#		Makefile handcrafted by tTh
#

all:	guinness guinnessd

# ---------------------------------------------------------

H_DEP =	broadcast.h commands.h defines.h guinnessd.h printlog.h 	\
	xmem.h clients.h config.h drinks.h lists.h tools.h

COPT =	-Wall -g

D_OBJS = xmem.o broadcast.o printlog.o lists.o tools.o drinks.o		\
	commands.o clients.o

C_OBJS = xmem.o tools.o

D_LIBS  = -lpthread -lcrypt

# ---------------------------------------------------------
#
#	Main program
#

guinnessd:	guinnessd.o Makefile $(D_OBJS)
	gcc -g $<   $(D_OBJS)  $(D_LIBS)     -o $@

guinnessd.o:	guinnessd.c Makefile $(H_DEP)
	gcc $(COPT) -c $<  

#

guinness:	guinness.o Makefile $(D_OBJS)
	gcc -g $<   $(C_OBJS)  $(D_LIBS)     -o $@

guinness.o:	guinness.c Makefile $(H_DEP)
	gcc $(COPT) -c $<  

# ---------------------------------------------------------
#
#	modules needed by the daemon
#

broadcast.o:	broadcast.c Makefile $(H_DEP)
	gcc $(COPT) -c $<
printlog.o:	printlog.c Makefile $(H_DEP)
	gcc $(COPT) -c $<
lists.o:	lists.c Makefile $(H_DEP)
	gcc $(COPT) -c $<
xmem.o:		xmem.c Makefile $(H_DEP)
	gcc $(COPT) -c $<
tools.o:	tools.c Makefile $(H_DEP)
	gcc $(COPT) -c $<
drinks.o:	drinks.c Makefile $(H_DEP)
	gcc $(COPT) -c $<
commands.o:	commands.c Makefile $(H_DEP)
	gcc $(COPT) -c $<
clients.o:	clients.c Makefile $(H_DEP)
	gcc $(COPT) -c $<

# ---------------------------------------------------------



/*
 * guinnessd
 * architecture clients/serveur guinness : broadcast messages
 * Thomas Nemeth -- le 15 juin 2001
 *
 */


#ifndef GUINNNESS_BROADCAST
#define GUINNNESS_BROADCAST

#include "guinnessd.h"

#define QUITTER   1
#define TOURNEE   2
#define MESSAGE   3


int broadcast (int t, const char *f, const char *m);
int new_message (long cid);
int get_broadcastid ();
void get_broadcastmsg (int *t, char **f, char **m);
int send_broadcast (int socket_service, userinfos *infos);

#endif

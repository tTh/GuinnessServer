/*
 * printlog
 * architecture clients/serveur guinness
 * Thomas Nemeth / Arnaud Giersch -- le 23 aout 2001
 *
 */

#include <stdio.h>
#include <time.h>
#ifdef SunOS
#include <sys/varargs.h>
#else
#include <stdarg.h>
#endif
#include <string.h>
#include "defines.h"
#include "printlog.h"


extern FILE *outerr;
extern FILE *logfile;


void printlog (log_level loglevel, const char *format, ...) {
    va_list    ap;
    FILE      *stream = logfile;
    time_t     now;
    struct tm *today;
    char       date_time[MAXSTRLEN];

    va_start (ap, format);

    switch (loglevel) {
        case LOG_NOTIFY:
            stream = logfile;
            break;
        case LOG_ERROR:
            stream = outerr;
            break;
        default:
            stream = outerr;
            break;
    }

    time (&now);
    today = localtime (&now);
    memset (date_time, 0, MAXSTRLEN);
    strftime (date_time, MAXSTRLEN - 1, "%a %d %b %Y %T", today);
    fprintf  (stream, "guinnessd: %s - ", date_time);
    vfprintf (stream, format, ap);

    va_end (ap);
}


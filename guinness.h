/*
 * guinness
 * architecture clients/serveur guinness.
 * Thomas Nemeth -- le 15 juin 2001
 *
 */

#ifndef GUINNESS_CLIENT
#define GUINNESS_CLIENT

#define CONFIG_FILE    ".guinnessrc"

#define DEFAULT_SERVER "127.0.0.1"
#define DEFAULT_PROMPT "> "


#endif

# Guinness Server

[Official Website of Guinnessd](http://tnemeth.free.fr/projets/guinness-server.html)

## Origine :

La version d'origine a un **gros** souci :
le `Makefile` tourne désepérement
[en boucle](boucle-de-make.txt) !
J'ai donc décidé, pour occuper mes longues journées de
retraite sanitaire, de sortir la disqueuse et de tenter de trouver 
le bug. Et ça ne va pas être simple, le Makefile d'origine est 
**très** convolué...

Par la suite, j'ai l'intention d'y apporter quelques modifications
que je trouverais à mon gout. Elles seront décrites plus bas
dans la page.

## Méthodologie :

Pour commencer, je vais utiliser un mélange de [rache](https://la-rache.com/)
et de [poudre verte](https://poudreverte.org/), additionné d'un peu de
jus de citron pour le goût : importer les fichiers un à un jusqu'à ce
que ça marche. Et par la suite, remttre les choses au clair.

Première étape : tenter de compiler le `main()` du serveur. Oké, il demande
un certain nombre de `.h`, que je m'empresse de lui fournir à partir de
l'original. C'est au moment où il me demande le `config.h` que j'allume
la disqueuse, je n'ai qu'un `config.h.in` sous la main. Qu'à cela ne tienne,
_kraftons_ vite ce fichier manquant à la main.

Voilà finalement, avec cette méthode (que je n'hésite à appeler le 
goutte-à-goutte), j'ai reconstitué un Makefile qui fonctionne et
compile le client et le serveur.

## Résultat :

Et voilà une session typique de remote-picole :

```
tth@lubitel:~/Devel/GuinnessServer$ ./guinnessd 
guinnessd: Sat 28 Mar 2020 15:21:35 - Impossible d'ouvrir le repertoire [/var/tmp/drinks.d]
guinnessd: Sat 28 Mar 2020 15:21:35 - Boissons disponibles :
guinnessd: Sat 28 Mar 2020 15:21:35 - 0 : guinness
guinnessd: Sat 28 Mar 2020 15:21:35 - Serveur en attente de connexions (port 1664)...
guinnessd: Sat 28 Mar 2020 15:22:18 - Connexion acceptee...
guinnessd: Sat 28 Mar 2020 15:22:18 - Connexion entrante : 127.0.0.1 localhost
guinnessd: Sat 28 Mar 2020 15:22:18 - Ports (loc/dist)   : 1664 / 47647
guinnessd: Sat 28 Mar 2020 15:22:18 - Utilisateur        : [tth]
guinnessd: Sat 28 Mar 2020 15:22:18 - Boisson preferee   : [guinness]
guinnessd: Sat 28 Mar 2020 15:22:18 - Message de logout  : [Bye bye...]
guinnessd: Sat 28 Mar 2020 15:22:18 - Date de connexion  : [Sat 28 Mar 2020 15:22:18]
guinnessd: Sat 28 Mar 2020 15:22:19 - Broadcast pour [tth] : MESSAGE
guinnessd: Sat 28 Mar 2020 15:22:19 -    de [---] : tth a rejoint le serveur de Guinness.
```


« Ah, c'est l'heure d'aller boire une bière, et comme il ne faut jamais
boire le ventre vide, je vais manger une Guinness avant. »


```
tth@lubitel:~/Devel/GuinnessServer$ ./guinness -u tth
Serveur     : [127.0.0.1]       Port    : [1664]
Utilisateur : [tth]             Boisson : [guinness]    Logout : [Bye bye...]
Pr�fixe     : [/]
-+- Connexion acceptee. -+-

Bienvenue sur le serveur de Guinness.
> <---> tth a rejoint le serveur de Guinness.
> /1
       .____,ooo____.
  ,d#MMMMMMMMMMMMMMMMMM#o.
 |MMMMMMMMMMMMMMMMMMMMMMMM
 |MMMMMMMMMMMMMMMMMMMMMMMM
 |MMMMMMMMMMMMMMMMMMH#*#**
 |M'"""""""""""""'`
 |M.
 `ML
  HP        ##o#
  |L        TMP]
   M  .      *&'    .   `
   |, |dL.?-\.~b   \:^  |
   `|   ` `  ' ``      ,
    H                  `
    |.
    `|                 |
     M                ,'
     |                |
     |,               |
     ||               |
     J'               |
     M.               J|
     HM\\           -':|
     `"=+\&#HMH#*??v/''
          `""""`""
> 

```
## Modifications

* ajouter une variable d'environnement `DRINKS_DIR` pour spécifier l'emplacement
de la réserve de picole.
* modifier la gestion des fichiers de picole afin d'avoir une description
succinte dans le listing du bar.





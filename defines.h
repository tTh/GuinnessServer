/*
 * defines
 * Creation et manipulation de sockets
 * Thomas Nemeth -- le 15 juin 2001
 *
 */

#ifndef GUINNESS_DEFINES
#define GUINNESS_DEFINES

#include <assert.h>

#define DEFAULT_SERVER_PORT 1664
#define DEFAULT_DRINK  "guinness"
#define DEFAULT_LOGOUT "Bye bye..."
#define DEFAULT_PSEUDO "toto"

#define SEP "\n"

#define MAXSTRLEN           4096

#ifndef TRUE
#define TRUE                1
#define FALSE               0
#endif

/* Important macros */
#define MY_ALLOC(var, nb, type) \
    ((var) = malloc((nb)*sizeof(type)), assert((var) != NULL))

#endif

/*
 * xmem
 * architecture clients/serveur guinness : gestion memoire
 * Thomas Nemeth -- le 24 aout 2001
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "xmem.h"


void *xmalloc (size_t taille) {
    void *ret = malloc (taille);
    if (ret == NULL) {
        perror ("malloc() ");
        exit (-1);
    } else
        return ret;
}

char *xstrdup (const char *chaine) {
    char *ret = strdup (chaine);
    if (ret == NULL) {
        perror ("strdup() ");
        exit (-1);
    } else
        return ret;
}

/*
 * tools
 * Creation et manipulation de sockets
 * Thomas Nemeth -- le 15 juin 2001
 *
 */

#ifndef SOCKETS_TOOLS
#define SOCKETS_TOOLS

#include <netinet/in.h>

int ouvre_socket   (int port, char *adr_ip, struct sockaddr_in *ptr_adresse);
int connect_server (const char *machine, int port);
int install_server (int port, char *adr_ip, struct sockaddr_in *ptr_adresse);
int read_infos (int socket_data, char *infos);
int send_infos (int socket_data, const char *infos);
void get_sock_infos (int sock, char **nom, char **adr, int *prl, int *prd);

#endif

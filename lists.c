/*
 * lists
 * architecture clients/serveur guinness
 * Thomas Nemeth -- le 15 juin 2001
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "defines.h"
#include "xmem.h"
#include "lists.h"


void add_elt (Elt **elt, const char *nom) {
    Elt *e;

    if (*elt == NULL) {
        *elt = xmalloc ((1) * sizeof (Elt));
        (*elt)->nom = xstrdup (nom);
        (*elt)->next = NULL;
    } else {
        e = *elt;
        while (e->next != NULL) e = e->next;
        e->next = xmalloc ((1) * sizeof (Elt));
        e       = e->next;
        e->nom  = xstrdup (nom);
        e->next = NULL;
    }
}


void remove_elt (Elt **elt, const char *nom) {
    Elt  *e = *elt, *p = NULL;
    int   res = FALSE;
    char *rt, *text ;

    do {
        if (e != NULL) {
            if ((rt = strstr (e->nom, SEP)) != NULL) {
                text = xmalloc (rt - e->nom + 2);
                memset (text, 0, rt - e->nom + 2);
                strncpy (text, e->nom, rt - e->nom);
#ifdef DEBUG
                printf ("Comparaison (l) de [%s] avec [%s]\n", text, nom);
#endif
                res = (strcmp (text, nom) == 0);
                free (text);
            } else {
#ifdef DEBUG
                printf ("Comparaison (c) de [%s] avec [%s]\n", e->nom, nom);
#endif
                res = (strcmp (e->nom, nom) == 0);
            }
            if (res == FALSE) {
                p = e;
                e = e->next;
            }
        }
    } while ( (e != NULL) && (res != TRUE) );

    if (e == NULL) return;
    if (e == *elt) *elt = e->next;
    if (p) p->next = e->next;
    free (e->nom);
    free (e);
}


void remove_elt_n (Elt **elt, int i) {
    Elt *e = *elt, *p = NULL;
    int  n = 0;

    while ( (e != NULL) && (n != i) ) {
        n++;
        e = e->next;
    }
    if (e == NULL) return;
    if (e == *elt) *elt = e->next;
    if (p) p->next = e->next;
    free (e->nom);
    free (e);
}


int get_nb_elts (Elt *elt) {
    Elt *e = elt;
    int  n = 0;

    while (e != NULL) {
        n++;
        e = e->next;
    }

    return n;
}


char *elt_number (Elt *elt, int i) {
    Elt *e = elt;
    int  n = 0;

    while ( (e != NULL) && (n != i) ) {
        n++;
        e = e->next;
    }
    if (e == NULL) return NULL;

    return e->nom;
}


int exist_elt (Elt *elt, const char *nom) {
    Elt  *e   = elt;
    int   res = FALSE;
    char *rt, *text ;

    do {
        if (e != NULL) {
            if ((rt = strstr (e->nom, SEP)) != NULL) {
                text = xmalloc (rt - e->nom + 2);
                memset (text, 0, rt - e->nom + 2);
                strncpy (text, e->nom, rt - e->nom);
#ifdef DEBUG
                printf ("Comparaison (l) de [%s] avec [%s]\n", text, nom);
#endif
                res = (strcmp (text, nom) == 0);
                free (text);
            } else {
#ifdef DEBUG
                printf ("Comparaison (c) de [%s] avec [%s]\n", e->nom, nom);
#endif
                res = (strcmp (e->nom, nom) == 0);
            }
            e = e->next;
        }
    } while ( (e != NULL) && (res != TRUE) );

    return res;
}


char *get_elt (Elt *elt, const char *nom) {
    Elt  *e = elt;
    int   res = FALSE;
    char *rt, *text ;

    do {
        if (e != NULL) {
            if ((rt = strstr (e->nom, SEP)) != NULL) {
                text = xmalloc (rt - e->nom + 2);
                memset (text, 0, rt - e->nom + 2);
                strncpy (text, e->nom, rt - e->nom);
#ifdef DEBUG
                printf ("Comparaison (l) de [%s] avec [%s]\n", text, nom);
#endif
                res = (strcmp (text, nom) == 0);
                free (text);
            } else {
#ifdef DEBUG
                printf ("Comparaison (c) de [%s] avec [%s]\n", e->nom, nom);
#endif
                res = (strcmp (e->nom, nom) == 0);
            }
            if (res == FALSE) e = e->next;
        }
    } while ( (e != NULL) && (res != TRUE) );

    if (e == NULL) return NULL;

    return e->nom;
}


void free_list (Elt **elt) {
    Elt *e = *elt, *p;

    while (e != NULL) {
        p = e;
        e = e->next;
        free (p->nom);
        free (p);
    }
    *elt = NULL;
}

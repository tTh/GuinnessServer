/*
 * clients
 * architecture clients/serveur guinness
 * Thomas Nemeth -- le 15 juin 2001
 *
 */


#ifndef GUINNESS_CLIENTS
#define GUINNESS_CLIENTS


void  add_client    (userinfos *infos);
void  remove_client (userinfos *infos);
int   rename_client (userinfos *infos, const char *new_nick);
char *list_clients  ();
char *infos_client  (int admin, const char *nick);


#endif

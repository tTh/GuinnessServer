/*
 * from config.h.in :
 *	architecture clients/serveur guinness : configuration.
 * 	homas Nemeth -- le 15 juin 2001
 *
   2020-03-28 : handcrafted by tth
 *
 */

#define VERSION		"0.4.7-tth"
#define OS_TYPE		"cp/m 68k"
#define COMPIL_DATE	__DATE__

#define DRINKS_DIR	"/var/tmp/drinks.d"


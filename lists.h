/*
 * lists
 * architecture clients/serveur guinness
 * Thomas Nemeth -- le 15 juin 2001
 *
 */


#ifndef GUINNESS_LISTS
#define GUINNESS_LISTS

typedef struct elt_t_ {
    char          *nom;
    struct elt_t_ *next;
} Elt;


void  add_elt      (Elt **elt, const char *nom);
void  remove_elt   (Elt **elt, const char *nom);
void  remove_elt_n (Elt **elt, int i);
int   get_nb_elts  (Elt  *elt);
char *elt_number   (Elt  *elt, int i);
int   exist_elt    (Elt  *elt, const char *nom);
char *get_elt      (Elt  *elt, const char *nom);
void  free_list    (Elt **elt);

#endif

/*
 * printlog
 * architecture clients/serveur guinness
 * Thomas Nemeth / Arnaud Giersch -- le 23 aout 2001
 *
 */


#ifndef PRINT_LOGS
#define PRINT_LOGS

enum log_level {LOG_NOTIFY, LOG_ERROR};
typedef enum log_level log_level;

void printlog (log_level loglevel, const char *format, ...);


#endif

/*
 * guinnessd
 * architecture clients/serveur guinness
 * Thomas Nemeth -- le 15 juin 2001
 *
 */


#ifndef GUINNESS_SERVER
#define GUINNESS_SERVER

#include <pthread.h>

#define CONFIG_FILE "/etc/guinnessdrc"

#define ECRITURE 0
#define LECTURE  1

typedef struct userinfos {
    char nom[MAXSTRLEN + 1];
    char prefb[MAXSTRLEN + 1];
    char logout[MAXSTRLEN + 1];
    char cxdate[MAXSTRLEN + 1];
    char host[MAXSTRLEN + 1];
    char ip[MAXSTRLEN + 1];
    int  port;
    int  admin;
    int  cold;
    int  comm[2]; /* Pas encore utilise */
} userinfos;


pthread_mutex_t mutex_broadcast;
pthread_mutex_t mutex_clients;


#endif

/*
 * pint
 * architecture clients/serveur guinness
 * Thomas Nemeth -- le 15 juin 2001
 *
 */


#ifndef GUINNESS_SERVER_PINT
#define GUINNESS_SERVER_PINT

char *pinte =
"       .____,ooo____.\n"
"  ,d#MMMMMMMMMMMMMMMMMM#o.\n"
" |MMMMMMMMMMMMMMMMMMMMMMMM\n"
" |MMMMMMMMMMMMMMMMMMMMMMMM\n"
" |MMMMMMMMMMMMMMMMMMH#*#**\n"
" |M'\"\"\"\"\"\"\"\"\"\"\"\"\"'`\n"
" |M.\n"
" `ML\n"
"  HP        ##o#\n"
"  |L        TMP]\n"
"   M  .      *&'    .   `\n"
"   |, |dL.?-\\.~b   \\:^  |\n"
"   `|   ` `  ' ``      ,\n"
"    H                  `\n"
"    |.\n"
"    `|                 |\n"
"     M                ,'\n"
"     |                |\n"
"     |,               |\n"
"     ||               |\n"
"     J'               |\n"
"     M.               J|\n"
"     HM\\\\           -':|\n"
"     `\"=+\\&#HMH#*??v/''\n"
"          `\"\"\"\"`\"\"\n";



#endif

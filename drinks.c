/*
 * drinks
 * architecture clients/serveur guinness
 * Thomas Nemeth -- le 15 juin 2001
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include "defines.h"
#include "xmem.h"
#include "printlog.h"
#include "lists.h"
#include "drinks.h"
#include "pint.h"

extern char *pinte;
extern char *chemin;
extern Elt  *drinks_list;
extern FILE *logfile;
extern FILE *outerr;


char *drinks_get_from_file (const char *nom) {
    struct stat  fileinfos;
    char         filename[MAXSTRLEN + 1];
    char        *datas;
    FILE        *fichier;

    snprintf (filename, MAXSTRLEN, "%s/%s", chemin, nom);
    if (stat (filename, &fileinfos) == -1) {
        printlog (LOG_ERROR, "Acces impossible au fichier [%s]\n",
                 filename);
        perror ("stat ");
        return NULL;
    }

    if ( (fichier = fopen (filename, "r")) == NULL) return NULL;
    datas = xmalloc ((fileinfos.st_size + 3) * sizeof (char));
    memset (datas, 0, fileinfos.st_size + 3);
    fread (datas, sizeof (char), fileinfos.st_size, fichier);
    fclose (fichier);

    return datas;
}


char *drinks_get (const char *nom) {
    char *breuvage = NULL;
    int   i;

    if (nom == NULL) return NULL;

    /* Element 0 (Guinness) disponible par defaut */
    if (strcmp (nom, "guinness")  == 0) breuvage = xstrdup (pinte);

    /* Sinon on cherche dans la liste des fichiers :
     * on commence a 1 pour eviter la pinte de Guinness qui est traitee
     * juste avant.
     */
    if (! breuvage) {
        for (i = 1 ; i < get_nb_elts (drinks_list) ; i++)
            if (strcmp (nom, elt_number (drinks_list, i)) == 0)
                breuvage = drinks_get_from_file (nom);
    }

    /* Dernier cas : on n'a rien trouve => pinte par defaut */
    if (! breuvage) breuvage = xstrdup (pinte);

    return breuvage;
}

void drinks_list_files (const char *path) {
    DIR           *rep;
    struct dirent *dirinfos;
    struct stat    fileinfos;
    int            nbfiles = 0;
    char           filename[MAXSTRLEN + 1];

    if ( (rep = opendir (path)) == NULL) {
        printlog (LOG_ERROR, "Impossible d'ouvrir le repertoire [%s]\n", path);
#ifdef DEBUG
        perror ("opendir ");
#endif
        return;
    }

    printlog (LOG_NOTIFY, "Liste des fichiers a integrer :\n");
    while ((dirinfos = readdir (rep)) != NULL) {
        snprintf (filename, MAXSTRLEN, "%s/%s", path, dirinfos->d_name);
        if (stat (filename, &fileinfos) == -1) {
            printlog (LOG_ERROR, "Acces impossible au fichier [%s]\n",
                     filename);
#ifdef DEBUG
            perror ("stat ");
#endif
        }
        if ( (dirinfos->d_name[0] != '.') &&
             ( (fileinfos.st_mode & S_IFMT) == S_IFREG) ) {
            printlog (LOG_NOTIFY, "   fichier : [%s]\n",
                     dirinfos->d_name);
            add_elt (&drinks_list, dirinfos->d_name);
            nbfiles++;
        }
    }
    printlog (LOG_NOTIFY, "Fin de la liste : %d fichier%s.\n",
             nbfiles, nbfiles > 1 ? "s" : "");
    closedir (rep);
}


void drinks_display_list () {
    int   i;
    char *breuvage;

    printlog (LOG_NOTIFY, "Boissons disponibles :\n");
    for (i = 0 ; i < get_nb_elts (drinks_list) ; i++) {
        breuvage = elt_number (drinks_list, i);
        printlog (LOG_NOTIFY, "%d\t: %s\n", i, breuvage);
    }
}

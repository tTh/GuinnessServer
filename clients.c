/*
 * clients
 * architecture clients/serveur guinness : clients list
 * Thomas Nemeth -- le 15 juin 2001
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include "defines.h"
#include "printlog.h"
#include "xmem.h"
#include "guinnessd.h"
#include "lists.h"
#include "tools.h"
#include "clients.h"


extern Elt *clients_list;

void add_client (userinfos *infos) {
    char *datas;
    int   size;

    size = strlen (infos->nom);
    size += strlen (infos->prefb);
    size += strlen (infos->cxdate);
    size += strlen (infos->host);
    size += strlen (infos->ip);
    size += 5 * strlen (SEP) + 10;

    datas = xmalloc (size + 1);

    sprintf (datas, "%s%s%s%s%s%s%s%s%s%s%d",
             infos->nom,    SEP,
             infos->prefb,  SEP,
             infos->cxdate, SEP,
             infos->host,   SEP,
             infos->ip,     SEP,
             infos->port);

    add_elt (&clients_list, datas);

#ifdef DEBUG
    printf ("Taille des elements :\n"
            "nom   : %d [%s]\n"
            "biere : %d [%s]\n"
            "date  : %d [%s]\n"
            "hote  : %d [%s]\n"
            "ip    : %d [%s]\n",
            strlen (infos->nom), infos->nom,
            strlen (infos->prefb), infos->prefb,
            strlen (infos->cxdate), infos->cxdate,
            strlen (infos->host), infos->host,
            strlen (infos->ip), infos->ip);

    printf ("Ajout des infos : size = %d -- [%s]\n", size, datas);
#endif

    free (datas);
}


void remove_client (userinfos *infos) {
    remove_elt (&clients_list, infos->nom);
}


int rename_client (userinfos *infos, const char *new_nick) {
    if (exist_elt (clients_list, new_nick)) return FALSE;
    remove_elt (&clients_list, infos->nom);
    snprintf (infos->nom, MAXSTRLEN, "%s", new_nick);
    add_client (infos);
    return TRUE;
}


char *list_clients () {
    char *list;
    char *lstelt;
    char *rt;
    char  text[MAXSTRLEN + 1];
    int   size;
    int   i;

    size = strlen ("Client(s) connecte(s) :\n");
    for (i = 0 ; i < get_nb_elts (clients_list) ; i++) {
        lstelt = elt_number (clients_list, i);
        rt = strstr (lstelt, SEP);
        strncpy (text, lstelt, rt - lstelt);
        text[rt - lstelt] = 0;
        size += 1 + strlen (text);
    }

    list = xmalloc ((size + 1) * sizeof (char));

    strcpy (list, "Client(s) connecte(s) :\n");
    for (i = 0 ; i < get_nb_elts (clients_list) ; i++) {
        lstelt = elt_number (clients_list, i);
        rt = strstr (lstelt, SEP);
        strncpy (text, lstelt, rt - lstelt);
        text[rt - lstelt] = 0;
        strcat (list, text);
        strcat (list, "\n");
    }

    return list;
}

char *infos_client (int admin, const char *nick) {
    char *datas;
    char *infos;
    char *token, *use, *saveptr;
    char *nom  = NULL, *biere = NULL, *date = NULL;
    char *host = NULL, *ip    = NULL, *port = NULL;
    int   size = 0;

    infos = get_elt (clients_list, nick);
    if (! infos) {
        datas = xmalloc (strlen (nick) + strlen (" : pseudo inconnu !\n"));
        sprintf (datas, "%s : pseudo inconnu !\n", nick);
        return datas;
    }

    use = xstrdup (infos);

    /* nom */
    token = strtok_r (use, SEP, &saveptr);
    if (token) nom = xstrdup (token);

    /* biere */
    token = strtok_r (NULL, SEP, &saveptr);
    if (token) biere = xstrdup (token);

    /* date */
    token = strtok_r (NULL, SEP, &saveptr);
    if (token) date = xstrdup (token);

    /* hote */
    token = strtok_r (NULL, SEP, &saveptr);
    if (token) host = xstrdup (token);

    /* ip */
    token = strtok_r (NULL, SEP, &saveptr);
    if (token) ip = xstrdup (token);

    /* port */
    token = strtok_r (NULL, SEP, &saveptr);
    if (token) port = xstrdup (token);

    size += strlen ("Pseudo    : ");
    size += strlen ("Biere     : ");
    size += strlen ("Connexion : ");
    size += 1 + (nom   ? strlen (nom)   : 1);
    size += 1 + (biere ? strlen (biere) : 1);
    size += 1 + (date  ? strlen (date)  : 1);
    if (admin) {
        size += strlen ("Hote      : ");
        size += strlen ("IP        : ");
        size += strlen ("Port      : ");
        size += 1 + (host ? strlen (host) : 1);
        size += 1 + (ip   ? strlen (ip)   : 1);
        size += 1 + (port ? strlen (port) : 1);
    }

    datas = xmalloc (size + 1);

    sprintf (datas,
             "Pseudo    : %s\n"
             "Biere     : %s\n"
             "Connexion : %s\n",
             nom   ? nom   : "-",
             biere ? biere : "-",
             date  ? date  : "-");
    if (admin)
        sprintf (datas, "%s"
                 "Hote      : %s\n"
                 "IP        : %s\n"
                 "Port      : %s\n",
                 datas,
                 host ? host : "-",
                 ip   ? ip   : "-",
                 port ? port : "-");

    if (nom)   free (nom);
    if (biere) free (biere);
    if (date)  free (date);
    if (host)  free (host);
    if (ip)    free (ip);
    if (port)  free (port);

    return datas;
}

/*
 * xmem
 * architecture clients/serveur guinness : gestion memoire
 * Thomas Nemeth -- le 24 aout 2001
 *
 */


#ifndef GUINNESS_XMEM
#define GUINNESS_XMEM

#include <string.h>
#include <strings.h>

void *xmalloc (size_t taille);
char *xstrdup (const char *chaine);


#define IS_NOT_GOOD(string) (! string || (strcmp (string, "") == 0))

#define SET_STRING(string, value) {\
    if (string) free (string); \
    string = xstrdup (value); }


#endif
